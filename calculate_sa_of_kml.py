'''
I, Sar-bear, make great things.
RAWR
'''

from lxml import etree
import pyproj
import shapely.geometry as sg
import glob
import os
from shapely.geometry import Polygon
from shapely.geometry import box
import csv
import pandas as pd
from datetime import datetime
import pyperclip

def kml_to_area(path_kml):
    # Read in data
    with open(path_kml, 'r') as fin:
        data_raw = etree.parse(fin)

    # Find coordinates
    for e in data_raw.iter():
        if 'coordinates' in e.tag:
            raw_coords = e.text

    # Convert to useful data:
    try:
        coords_raw = [[float(vv) for vv in v.split(',')[:2]] for v in raw_coords.strip().split(' ')]

        poly_raw = sg.Polygon(coords_raw)

        coords_aea = [p for p in pyproj.itransform(
            pyproj.Proj(init='EPSG:4326'),
            pyproj.Proj(proj='aea', lat_1=poly_raw.bounds[1], lat_2=poly_raw.bounds[3]),
            coords_raw)]

        area = sg.Polygon(coords_aea).area

    except UnboundLocalError as e:
        coords_raw = None

    # not sure if we actually need opti
    # optimus = pyproj.Transformer.from_crs('EPSG:4326', 'USA_Contiguous_Albers_Equal_Area_Conic')

    return (area, coords_aea)  # IN METERS


# now need to glob glob the google drive folder for all the kml files and run code on each file and save the sid (file name) and surface area and bounding box into a dict
# also now create bounding box AREA
# and use aea coords

search_folder = r"G:\My Drive\Heliolytics - Flight Data\2019\MaxSolar Sites"
site_folders_search = glob.glob(os.path.join(search_folder, '9*'))  # only want things with a side id

site_id = []  # keys
area = []  # values
area_list = []
for each_kml in site_folders_search:
    site_id = (each_kml.split(os.sep))[5].strip('.kml')
    area = str(kml_to_area(each_kml)[0])
    coords_aea = kml_to_area(each_kml)[1]
    polygon = Polygon(coords_aea)
    bounding_box = str(box(*polygon.bounds).area)
    # area dict contains all site ids and correspconding areas
    area_dict = dict(
        site_id=site_id,
        area_m=area,
        bounding_box_m=bounding_box
    )
    area_dict.update()
    area_list.append(area_dict)

# write to csv (can integrate copy/paste later)
df_sites = pd.DataFrame(area_list)
df_sites.columns = ['site_id', 'area_m', 'bounding_box_m']
df_sites.to_csv(f'MaxSolarSites.csv', sep='\t')

#Mike showed me all of this cool stuff which makes using google sheets a whole lot better and expands functionality
if area_list:
    # found things
    SEP_COL = '\t'
    SEP_ROW = '\r\n'

    col_headers = ['site_id', 'area_m', 'bounding_box_m']

    rows = [SEP_COL.join(['Acquired:', datetime.now().isoformat(timespec='seconds')])]

    rows += ['']

    rows += [SEP_COL.join(col_headers)]

    rows += [SEP_COL.join(each_dict.values()) for each_dict in area_list]
    output = SEP_ROW.join(rows)

    # pyperclip.copy(SEP_ROW.join(rows))

else:
    output = 'Nothing found '
    # pyperclip.copy('Nothing found at Stage 1')
print('Data ready, paste into spreadsheet! ;)')
ready_to_quit = False

while not ready_to_quit:
    pyperclip.copy(output)
    user_response = input('x to quit, anything else to copy again.').lower().strip()
    if user_response.startswith('x'):
        ready_to_quit = True


print('Done!')